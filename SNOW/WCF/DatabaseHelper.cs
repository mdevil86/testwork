﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MySql.Data.MySqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Json;

//nBBx1VcdEMUH
//u35417_plamit
//srv-25-msk.park-web.ru

namespace WCF
{
    public class DatabaseHelper : IDatabaseHelper
    {
        [Serializable]
        private class Product //Класс для получения данных из базы
        {
            private string art;
            private float price;
            private int count;

            public string getArt()
            {
                return art;
            }
            public float getPrice()
            {
                return price;
            }
            public float getCount()
            {
                return count;
            }

            public Product SetArt(string art)
            {
                this.art = art;
                return this;
            }
            public Product SetPrice(float price)
            {
                this.price = price;
                return this;
            }
            public Product SetCount(int count)
            {
                this.count = count;
                return this;
            }
        }

        public string ReadDatabase()
        {
            string result = "{}"; //создаем пустой объект JSON для возврата, если не будет ответа от сервера

            string connStr = "193.70.80.93;user=test;database=test;password=nBBx1VcdEMUH;";
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                //Пробуем подключиться к серверу
                conn.Open();
            }
            catch
            {
                //Если не получилось - возвращаем пустой объект
                return result;
            }

            string query = "SELECT art,price,count FROM goods";

            MySqlCommand command = new MySqlCommand(query, conn);

            MySqlDataReader reader = command.ExecuteReader();

            HashSet<Product> products = new HashSet<Product>();

            while (reader.Read())
            {
                products.Add(new Product()
                    .SetArt(reader.GetString(0))
                    .SetPrice(reader.GetFloat(1))
                    .SetCount(reader.GetInt32(2)));

            }

            // Сериализуем продукт в json
            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(HashSet<Product>));
            ser.WriteObject(stream1, products);

            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);

            result = sr.ReadToEnd();

            stream1.Close();

            sr.Close();

            reader.Close();

            conn.Clone();

            return result;
        }

        public bool WriteDatabase(string serializedData)
        {
            HashSet<Product> currentItems = new HashSet<Product>();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(HashSet<Product>));
            MemoryStream stream1 = new MemoryStream();
            stream1.Write(Encoding.ASCII.GetBytes(serializedData), 0, serializedData.Length);
            stream1.Position = 0;

            currentItems = (HashSet<Product>)ser.ReadObject(stream1);
            stream1.Close();

            string connStr = "server=127.0.0.1;user=root;database=u35417_plamit;password=;";
            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {
                //Пробуем подключиться к серверу
                conn.Open();
            }
            catch
            {
                //Если не получилось - возвращаем пустой объект
                return false;
            }

            foreach (Product currentItem in currentItems)
            {
                string query = "INSERT INTO goods (art,price,count) VALUES (\"" +
                    currentItem.getArt() + "\", " +
                    currentItem.getPrice() + ", " +
                    currentItem.getCount() + ");";

                MySqlCommand command = new MySqlCommand(query, conn);

                MySqlDataReader reader = command.ExecuteReader();
                reader.Close();
            }

            conn.Close();
            
            return true;
        }

        public bool WriteToFile(string serializedData)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter("C:\\products.json", true);
            file.WriteLine(serializedData);
            file.Close();
            return true;
        }

        public string ReadFile()
        {
            System.IO.StreamReader file = new System.IO.StreamReader("C:\\products.json");
            string serialized;
            serialized = file.ReadToEnd();
            file.Close();
            return serialized;
        }
    }
}
