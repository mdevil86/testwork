﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace WCF
{
    [ServiceContract]
    public interface IDatabaseHelper
    {
        [OperationContract]
        bool WriteToFile(String serializedData);

        [OperationContract]
        string ReadFile();

        [OperationContract]
        String ReadDatabase();

        [OperationContract]
        bool WriteDatabase(String serializedData);
    }
}
