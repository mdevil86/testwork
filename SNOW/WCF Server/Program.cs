﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using WCF;



namespace WCF_Server
{
    class Program
    {
        [Serializable]
        private class Good //Класс для получения данных из базы
        {
            private string art;
            private float price;
            private int count;

            public string getArt()
            {
                return art;
            }
            public float getPrice()
            {
                return price;
            }
            public float getCount()
            {
                return count;
            }

            public Good SetArt(string art)
            {
                this.art = art;
                return this;
            }
            public Good SetPrice(float price)
            {
                this.price = price;
                return this;
            }
            public Good SetCount(int count)
            {
                this.count = count;
                return this;
            }
        }

        static void Main(string[] args)
        {
            Type serviceType = typeof(DatabaseHelper);
            Uri serviceUri = new Uri("http://127.0.0.1:8080/");
            ServiceHost host = new ServiceHost(serviceType, serviceUri);
            host.AddDefaultEndpoints();
            host.Open();

            #region Output dispatchers listening
            foreach (Uri uri in host.BaseAddresses)
            {
                Console.WriteLine("\t{0}", uri.ToString());
            }
            Console.WriteLine();
            Console.WriteLine("Number of dispatchers listening : {0}", host.ChannelDispatchers.Count);
            foreach (System.ServiceModel.Dispatcher.ChannelDispatcher dispatcher in host.ChannelDispatchers)
            {
                Console.WriteLine("\t{0}, {1}", dispatcher.Listener.Uri.ToString(), dispatcher.BindingName);
            }
            Console.WriteLine();
            Console.WriteLine("Press <ENTER> to terminate Host");
            Console.ReadLine();
            #endregion
        }
    }
}
