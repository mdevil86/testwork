﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INPUT.Entity
{
    [Serializable]
    public class Product
    {
        private String art;
        private float price;
        private int count;

        public Product SetArt (String art)
        {
            this.art = art;
            return this;
        }

        public Product SetPrice (float price)
        {
            this.price = price;
            return this;
        }

        public Product SetCount (int count)
        {
            this.count = count;
            return this;
        }
    }
}
