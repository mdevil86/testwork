﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using INPUT.API;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;

namespace INPUT
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void AddProduct_Click(object sender, RoutedEventArgs e)
        {
            //Подключаемся к серверу
            DatabaseHelperClient client = new DatabaseHelperClient();
            //client.WriteToFile("test" + Art.Text);

            //Формируем продукт из введенных данных
            Entity.Product product = new Entity.Product()
                .SetArt(Art.Text)
                .SetCount(Int32.Parse(Count.Text))
                .SetPrice(float.Parse(Price.Text));

            //Сериализуем продукт в xml
/*            XmlSerializer xmlSerializer = new XmlSerializer(product.GetType());
            StringWriter textWriter = new StringWriter();
            xmlSerializer.Serialize(textWriter, product);*/

            // Сериализуем продукт в json
            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Entity.Product));
            ser.WriteObject(stream1, product);

            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);

            //Передаем xml в файл
            client.WriteToFile(sr.ReadToEnd());
            
            client.Close();
        }
    }
}
