﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace INPUT
{
    class ProductRules : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            float price;

            try
            {
                price = float.Parse((string)value);
            }
            catch
            {
                return new ValidationResult(false, "Недопустимые символы.");
            }

            if (price < 0) return new ValidationResult(false, "Стоимость не может быть отрицательной.");
            
            return new ValidationResult(true, null);

        }
    }
}
