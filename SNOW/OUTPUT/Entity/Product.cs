﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OUTPUT.Entity
{
    [Serializable]
    public class Product : INotifyPropertyChanged
    {
        private String art;
        private float price;
        private int count;

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            // Если кто-то на него подписан, то вызывем его
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public Product SetArt (String art)
        {
            this.art = art;
            RaisePropertyChanged("Art");
            return this;
        }

        public Product SetPrice (float price)
        {
            this.price = price;
            RaisePropertyChanged("Price");
            return this;
        }

        public Product SetCount (int count)
        {
            this.count = count;
            RaisePropertyChanged("Count");
            return this;
        }
    }
}
