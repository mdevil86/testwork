﻿using OUTPUT.API;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OUTPUT
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ReadFromFile();
            ReadFromDB();
        }

        public void ReadFromFile()
        {

            HashSet<Entity.Product> currentItems = new HashSet<Entity.Product>();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Entity.Product));
            MemoryStream stream1 = new MemoryStream();
            
            
            //Подключаемся к серверу
            DatabaseHelperClient client = new DatabaseHelperClient();
            string json = client.ReadFile();
            json = json.Replace("\r\n", "|");
            string[] substrings = json.Split('|');
            foreach(String good in substrings)
            {
                if (!good.Equals(""))
                {
                    stream1.Position = 0;
                    stream1.Write(Encoding.ASCII.GetBytes(good), 0, good.Length);
                    stream1.Position = 0;
                    
                    currentItems.Add((Entity.Product)ser.ReadObject(stream1));
                }
            }
            stream1.Close();

            
            foreach(Entity.Product item in currentItems)
            {

            }
        }

        public void ReadFromDB()
        {

        }
    }
}
